#!/usr/bin/env python
# coding: utf-8

# In[8]:


#create dataframe of room.geojson
import pandas_geojson as pdg
def read_geojson(file):
    geojson = pdg.read_geojson(file)
    df = geojson.to_dataframe()
    return df


# In[9]:


#read csv file data
def read_data(file):
    import pandas as pd
    wifi_data = pd.read_csv(file)

    return wifi_data



#basic dictionary with all datapoints keyed to each time
def create_dictionary(wifi_data):
    index=0
    dict1={}
    for item in wifi_data["_time"]:
        dict1[wifi_data["_time"][index]]=[]
        index+=1
    index=0
    for item in wifi_data["_time"]:
        dict1[wifi_data["_time"][index]].append([wifi_data["name"][index],wifi_data["user"][index],wifi_data["ssid"][index]])
        index+=1

    return dict1


#create list of dates
def create_date_list(wifi_data):
    day_list=[]
    for time in wifi_data["_time"]:
        if time[0:10] not in day_list:
            day_list.append(time[0:10])
    return day_list



#obtain list of all times corresponding to that date
def date_times_list(requested_date, wifi_data):
    date_list=[]
    for time in wifi_data["_time"]:
        if requested_date in time:
            date_list.append(time)

    return date_list



#extract just the hour from the timestamp
def get_hour(timestamp):
    return timestamp.split('T')[1].split(':')[0]

def sort_time_dict(date_list):
    from collections import Counter
    
    #make a list of each hour from 00 to 24
    hour_list=[]
    for item in date_list:
        hour=get_hour(item)
        hour_list.append(hour)
    
    #count up how many times each hour showed up in the entire day's log
    hour_count = Counter(hour_list)
    hour_count_dict=dict(hour_list)
    
    #sort times numerically
    sorted_time_dict = sorted(hour_count.items())
    return sorted_time_dict



#finds out how many users connected to an access point each hour
def get_hour(timestamp):
    return timestamp.split('T')[1].split(':')[0]

def sort_hourly_dict(dict1, requested_date):    
    grouped_by_hour = {}
    for key, values in dict1.items():
        if requested_date in key:
            hour = get_hour(key)
            if hour not in grouped_by_hour:
                grouped_by_hour[hour] = []
            grouped_by_hour[hour].extend(values)
    
    sorted_hourly_dict = sorted(grouped_by_hour.items())
    return sorted_hourly_dict




#finds out how many PEOPLE (netids) are in the building each hour
def filter_repeats(sorted_hourly_dict):
    occupancy_hourly_dict={}
    for key,values in sorted_hourly_dict:
        value_list=[]
        occupancy_hourly_dict[key]=[]
        for value in values:
            if value[1] not in value_list:
                value_list.append(value[1])
        occupancy_hourly_dict[key]=value_list
        
    return occupancy_hourly_dict




def calc_num_people(occupancy_hourly_dict):
    import matplotlib.pyplot as plt
    import numpy as np
    
    #actual number of people in room
    key_list=[]
    value_list=[]
    for key, value in occupancy_hourly_dict.items():
        key_list.append(key)
        value_list.append(len(value))
    
    hours=np.array(key_list)
    number_of_people=np.array(value_list)

    return hours, number_of_people





#create a dictionary using hours as key and number of people as value
def create_occupancy_hourly_dict(hours, number_of_people):
    temp_dict={}
    index=0
    for hour in hours:
        temp_dict[hour]=number_of_people[index]
        index+=1
    
    #if an hour is not logged, add it to dictionary but make occupancy count 0
    all_hours = [str(hour).zfill(2) for hour in range(24)]
    for hour in all_hours:
        if hour not in hours:
            temp_dict[hour] = 0
    
    final_occupancy_per_hour=sorted(temp_dict.items())
    return final_occupancy_per_hour




#separate by rooms
def separate_by_rooms(sorted_hourly_dict):
    room_hourly_dict={}
    for key,values in sorted_hourly_dict:
        value_list=[]
        id_storage=[]
        room_hourly_dict[key]=[]
        for value in values:
            if value[1] not in id_storage:
                value_list.append(value[0])
                id_storage.append(value[1])
                room_hourly_dict[key].append(value_list)
            value_list=[]
    
    return room_hourly_dict




#for each hour, determine how many people in each room
def total_room_counts(room_hourly_dict):
    total_room_counts = {}
    for hour, rooms in room_hourly_dict.items():
        room_count = {}
        for room_list in rooms:
            for room in room_list:
                room_count[room] = room_count.get(room, 0) + 1
        total_room_counts[hour]=room_count
    total_room_counts=sorted(total_room_counts.items())
    room_count=sorted(room_count.items())
    return total_room_counts




#organize so that room/ap name is the key
def organize_room_dict(total_room_count):    
    room_dict = {}
    
    for hour, room_data in total_room_count:
        for room, count in room_data.items():
            if room not in room_dict:
                room_dict[room] = {}
            room_dict[room][hour] = count
    
    return room_dict





#count number of people in the room/ap_name
def count_people_in_room(room_name):
    room1=room_dict[room_name]
    hours=[]
    number_of_people=[]
    all_hours = [str(hour).zfill(2) for hour in range(24)]
    for hour in all_hours:
        if hour not in room1:
            room1[hour] = 0
    room1=sorted(room1.items())
    for hour, number in room1:
        hours.append(hour)
        number_of_people.append(number)
    
    return number_of_people






#color gradient, generalized for all rooms

def create_room_gradient(number_of_people):
    color_grad=[]
    
    for item in number_of_people:
        if item < 5:
            color_grad.append("#44ce1b")
        elif item < 10:
            color_grad.append("#bbdb44")
        elif item < 15:
            color_grad.append("#f7e379")
        elif item < 20:
            color_grad.append("#f2a134")
        elif item < 25:
            color_grad.append("#ec771e")
        elif item <= 30:
            color_grad.append("#e51f1f")
    
    return color_grad


# In[10]:


def main(date_picker_value):
    #lets automate this, shall we?
    
    #read and sort data
    df=read_geojson("/app/bostock_rooms.geojson")
    wifi_data=read_data("/csv/june11_june17.csv")
    day_list=create_date_list(wifi_data)
    requested_date=date_picker_value
    date_list=date_times_list(requested_date, wifi_data)
    dict1=create_dictionary(wifi_data)
    sorted_time_dict=sort_time_dict(date_list)
    
    #building occupancy
    sorted_hourly_dict=sort_hourly_dict(dict1, requested_date)
    occupancy_hourly_dict=filter_repeats(sorted_hourly_dict)
    hours_and_people=calc_num_people(occupancy_hourly_dict)
    hours=hours_and_people[0]
    number_of_people=hours_and_people[1]
    final_occupancy_per_hour=create_occupancy_hourly_dict(hours, number_of_people)
    
     #room specific
    room_hourly_dict=separate_by_rooms(sorted_hourly_dict)
    total_room_count=total_room_counts(room_hourly_dict)
    room_dict=organize_room_dict(total_room_count)
    color_grad=create_room_gradient(number_of_people)

    return room_dict, df


# In[12]:


def create_heatmap(requested_date):
    #CHANGE DATE TO CREATE HEAT MAP FOR REQUESTED DATE
    data=main(requested_date)
    room_data=data[0]
    df=data[1]
    
    
    #shortens room string to just the room number
    updated_room_data={}
    for key, value in room_data.items():
        room_num=key.split('-')[1]
        updated_room_data[room_num]=value
    
    
    #sums hourly occupancies for full day occupancy
    daily_room_data={}
    for key, nested_dict in updated_room_data.items():
        total_sum = sum(nested_dict.values())
        daily_room_data[key] = total_sum              #might cause issues, repeat occupancy counts
    
    
    
    #assigns summed occupancy to each room
    import numpy as np
    def calculate_day_occupancy(room_number):
        if room_number in daily_room_data:
            return daily_room_data[room_number]
        else:
            return 0    #could also do np.nan for null value
    df['day_occupancy'] = df['properties.ROOM_NUM'].apply(calculate_day_occupancy)
    
    
    
    
    #save the altered room_geojson
    import json
    with open("/app/bostock_rooms.geojson", 'r') as f:
        room_geojson = json.load(f)
        
    for i, occupancy in enumerate(df['day_occupancy']):
        room_geojson['features'][i]['properties']['room_occupancy'] = occupancy
    
    with open('/app/output/updated_room_daycounts.geojson', 'w') as f:
        json.dump(room_geojson, f)
    
    # print(room_geojson)
    
    
    #Create heat map!
    
    
    
    #Interact with these!!!!
    BETA = 0.1
    ADD_MARKERS = False
    
    COLD_COLOR = 'green'
    WARM_COLOR = 'yellow'
    HOT_COLOR = 'red'
    
    
    def convert_linestring_to_polygon(feature):
        coordinates = feature['geometry']['coordinates']
        if coordinates[0] != coordinates[-1]:
            coordinates.append(coordinates[0])  # Close the loop
        feature['geometry']['type'] = 'Polygon'
        feature['geometry']['coordinates'] = [coordinates]
        return feature
    
    import pandas as pd

    csv_data = pd.read_csv('/csv/june11_june17.csv')
    
    csv_data['ROOM_NUM'] = csv_data['name'].str.extract(r'bostock-(\d+)-')[0]
    room_counts = csv_data['ROOM_NUM'].value_counts().to_dict()
    room_counts = {room: count // (1/BETA) for room, count in room_counts.items()}
    
    
    with open('/app/bostock1_floorplan.geojson') as f:  #Remember, this is the file that has the room numbers. Do NOT alter this file!
        geojson_data = json.load(f)
    
    
    for feature in geojson_data['features']:
        room_num = feature['properties'].get('ROOM_NUM')
        if room_num and room_num in room_counts:
            feature['properties']['occurrences'] = room_counts[room_num]
        else:
            feature['properties']['occurrences'] = 0
    
    # Defining the color scale
    from branca.colormap import LinearColormap
    max_occurrences = max(room_counts.values()) if room_counts else 0
    colormap = LinearColormap([COLD_COLOR, WARM_COLOR, HOT_COLOR], vmin=0, vmax=100)  #Mess around with the colors
    
    # Define the style function for Polygons
    def style_function(feature):
        room_occupancy = feature['properties'].get('room_occupancy', 0)
        return {
            'fillColor': colormap(room_occupancy),
            'color': 'brown', 
            'weight': 2,
            'fillOpacity': 0.9
        }
    
    # Filter the GeoJSON data to include only the features with a non-null ROOM_NUM
    room_features = [
        feature for feature in room_geojson['features']
        if feature['properties'].get('ROOM_NUM') is not None
    ]
    
    all_features = [
        feature for feature in geojson_data['features']
    ]
    
    # Convert LineString features to Polygon
    converted_features = [
        convert_linestring_to_polygon(feature)
        for feature in room_features
        if feature['geometry']['type'] == 'LineString'
    ]
    
    # Update the filtered GeoJSON with converted features
    for i, feature in enumerate(converted_features):
        if feature['geometry']['type'] == 'LineString':
            room_features[i] = converted_features.pop(0)
    
    
    all_geojson = {
        "type": "FeatureCollection",
        "features": all_features
    }
    
    # Create a map centered on the feature's coordinates
    import folium
    map_center = [36.002951920099044, -78.93814869328321]  # Adjust these coordinates as needed
    m = folium.Map(location=map_center, 
                   zoom_start=19, 
                   title='Room Occurances Heatmap of Bostock First Floor')
    
    folium.GeoJson(
        all_geojson
    ).add_to(m)
    
    # Add the filtered GeoJSON layer with styling
    folium.GeoJson(
        room_geojson,
        style_function=style_function
    ).add_to(m)
    
    if ADD_MARKERS:
        for feature in room_features:
            coords = feature['geometry']['coordinates'][0]
            centroid_lat = sum(point[1] for point in coords) / len(coords)
            centroid_lon = sum(point[0] for point in coords) / len(coords)
            folium.Marker(
                location=[centroid_lat, centroid_lon],
                popup=f"Room #: {feature['properties'].get('ROOM_NUM')} \n"
                        f"Occupancy: {int(feature['properties'].get('room_occupancy'))}",
            ).add_to(m)
    
    colormap.add_to(m)
    
    
    # Save the map to an HTML file
    heatmap_filename = f"heatmap_{requested_date}.html"
    m.save(heatmap_filename)
    
    return heatmap_filename

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python create_heatmap_bostock.py <date>")
        sys.exit(1)
    
    requested_date = sys.argv[1]
    heatmap_path = create_heatmap(requested_date)
    print(heatmap_path)


